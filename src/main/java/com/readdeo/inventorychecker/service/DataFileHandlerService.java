package com.readdeo.inventorychecker.service;

import com.readdeo.inventorychecker.model.JobFile;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

@Component
public class DataFileHandlerService {

    @Value("${file.upload-dir}")
    private String uploadPath = "${readdeo.app.jwtSecret}";

    public JSONObject getTableSampleCSV(JobFile jobFile) {
        JSONObject tableSampleJSON = new JSONObject();

        String filePath = uploadPath + jobFile.getFileName();
        File csvData = new File(filePath);

        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(csvData));
            String text = null;
            int currentLine = 0;
            int lastLineToParse = 4;
            String[] sampleRows = new String[lastLineToParse];

            while ((text = reader.readLine()) != null && currentLine <= lastLineToParse) {
                if (currentLine == 0) {
                    tableSampleJSON.put("header", text);
                } else {
                    sampleRows[currentLine - 1] = text;
                }

                currentLine++;
            }
            tableSampleJSON.put("data", sampleRows);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
        }
        return tableSampleJSON;
    }

    public JSONObject getCategoriesFromColumnCSV(JobFile jobFile, int columnId) {
        String filePath = uploadPath + jobFile.getFileName();

        JSONObject categoriesJSON = new JSONObject();

        ArrayList<String> categories = new ArrayList<String>();

        try {

            Reader reader = Files.newBufferedReader(Paths.get(filePath));
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);

            for (CSVRecord csvRecord : csvParser) {
                String data = csvRecord.get(columnId);
                if (!categories.contains(data)) {
                    categories.add(data);
                }
            }

        } catch (IOException e) {
            System.out.println(e);
        }

        categoriesJSON.put("categories", categories);
        return categoriesJSON;
    }
}
