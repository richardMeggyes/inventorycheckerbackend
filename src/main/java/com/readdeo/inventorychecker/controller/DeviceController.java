package com.readdeo.inventorychecker.controller;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/device")
@CrossOrigin
public class DeviceController {

    // TODO LINK SZAR
    private String serviceUrl = "http://localhost:8082/device-management";

    @GetMapping
    public String getDevices() {
        JSONArray devicesJSON = sendGetRequestJSONArray("?user=1");
        return new JSONObject().put("status", "success").put("devices", devicesJSON.toString()).toString();
    }

    @PutMapping
    public String newDevice() {
        sendPutRequestArr(
                "?user=1&uuid=asd&name=devicename&deviceholdersname=Sanya");

        return "{\"status\":\"success\"}";
    }

    private JSONObject sendGetRequest(String url) {
        RestTemplate restTemplateComic = new RestTemplate();
        String response = restTemplateComic.getForObject(serviceUrl + url, String.class);
        System.out.println("RESPONSE: " + response);
        return new JSONObject(response);
    }

    private JSONArray sendGetRequestJSONArray(String url) {
        RestTemplate restTemplateComic = new RestTemplate();
        String response = restTemplateComic.getForObject(serviceUrl + url, String.class);
        System.out.println("RESPONSE: " + response);
        return new JSONArray(response);
    }

    private void sendPutRequestArr(String url) {
        RestTemplate restTemplateComic = new RestTemplate();
        restTemplateComic.put(serviceUrl + url, String.class);
    }
}
