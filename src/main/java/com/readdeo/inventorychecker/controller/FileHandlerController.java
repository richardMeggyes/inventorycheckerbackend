package com.readdeo.inventorychecker.controller;

import com.readdeo.inventorychecker.model.JobFile;
import com.readdeo.inventorychecker.model.auth.User;
import com.readdeo.inventorychecker.repository.JobFileRepository;
import com.readdeo.inventorychecker.repository.UserRepository;
import com.readdeo.inventorychecker.service.DataFileHandlerService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/file")
public class FileHandlerController {

    @Autowired
    private DataFileHandlerService dataFileHandlerService;

    @Autowired
    private UserRepository ur;

    @Autowired
    private JobFileRepository jobFileRepository;

    @Value("file.upload-dir")
    private String uploadPath;

    //Save the uploaded file to this folder
    private static String UPLOADED_FOLDER = "./uploads/";

    @PostMapping("/getcategoriesfromcolumn") // //new annotation since 4.3
    public String getCategoriesFromColumn(@RequestParam("fileId") Long fileId,
                                          @RequestParam("columnId") int columnId,
                                          Authentication authentication) {
        User user = ur.findByUsername(authentication.getName()).orElse(null);

        JobFile jobFile = jobFileRepository.findJobFileById(fileId);

        if (jobFile.getUser() == user) {
            return dataFileHandlerService.getCategoriesFromColumnCSV(jobFile, columnId).toString();
        } else {
            return new JSONObject().put("ERROR", "Unauthorized").toString();
        }
    }
}
