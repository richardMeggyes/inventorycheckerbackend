package com.readdeo.inventorychecker.controller;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/debug")
@CrossOrigin
public class DebugController {

    @GetMapping
    public String getDevices() {
        return new JSONObject().put("status", "success").toString();
    }
}
