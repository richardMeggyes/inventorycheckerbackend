package com.readdeo.inventorychecker.controller;

import com.readdeo.inventorychecker.model.JobFile;
import com.readdeo.inventorychecker.model.auth.User;
import com.readdeo.inventorychecker.repository.JobFileRepository;
import com.readdeo.inventorychecker.repository.UserRepository;
import com.readdeo.inventorychecker.service.DataFileHandlerService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@CrossOrigin
@RestController
public class UploadController {

    @Autowired
    private DataFileHandlerService dataFileHandlerService;

    @Autowired
    UserRepository ur;

    @Autowired
    JobFileRepository jobFileRepository;

//    @Autowired
//    JobRepository jobRepository;

    //Save the uploaded file to this folder
    private static String UPLOADED_FOLDER = "./uploads/";

    @PostMapping("/upload") // //new annotation since 4.3
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
                                   @RequestParam("dataType") String dataType,
                                   @RequestParam("jobId") String jobIdStr,
                                   RedirectAttributes redirectAttributes,
                                   Authentication authentication) {
        User user = ur.findByUsername(authentication.getName()).orElse(null);

//        Job job;
//
//        if (!jobIdStr.equals("undefined")) {
//            Long jobId = Long.parseLong(jobIdStr);
//            job = jobRepository.findTopByIdAndUser(jobId, user);
//        } else {
//            job = new Job();
//        }


        String uuid = UUID.randomUUID().toString().replace("-", "");

        JSONObject answer = new JSONObject();

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            answer.put("status", "ERROR");
            answer.put("errorMessage", "File is empty");
            return answer.toString();
        }
        try {
            // Get the file and save it somewhere
            String fileName = uuid + "_" + file.getOriginalFilename();
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + fileName);
            Files.write(path, bytes);

            JobFile newJobFile = new JobFile();
            newJobFile.setFileName(fileName);
            newJobFile.setUser(user);
            jobFileRepository.save(newJobFile);

//            System.out.println("DATATYPE: " + dataType);
//            if (dataType.equals("reference")) {
//
//                job.setReferenceFile(newJobFile);
//                jobRepository.save(job);
//                answer.put("jobId", job.getId());
//            } else if (dataType.equals("costCenter")) {
//                job.setCcFile(newJobFile);
//                jobRepository.save(job);
//                answer.put("jobId", job.getId());
//            } else {
//                answer.put("status", "ERROR");
//                answer.put("errorMessage", "DataType doesnt exist");
//                return answer.toString();
//            }

            JSONObject dataSample = dataFileHandlerService.getTableSampleCSV(newJobFile);
            answer.put("header", dataSample.get("header"));
            answer.put("data", dataSample.get("data"));
            answer.put("fileId", newJobFile.getId());
        } catch (IOException e) {
            e.printStackTrace();
            answer.put("status", "ERROR");
            answer.put("errorMessage", "Unhandled exception while saving file");
            return answer.toString();
        }
        answer.put("status", "success");
        return answer.toString();
    }
}
