package com.readdeo.inventorychecker.controller;


import com.readdeo.inventorychecker.model.Job;
import com.readdeo.inventorychecker.model.JobFile;
import com.readdeo.inventorychecker.model.auth.User;
import com.readdeo.inventorychecker.repository.JobFileRepository;
import com.readdeo.inventorychecker.repository.JobRepository;
import com.readdeo.inventorychecker.repository.UserRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping("/job")
public class JobController {

    @Autowired
    UserRepository ur;

    @Autowired
    JobRepository jobRepository;

    @Autowired
    JobFileRepository jobFileRepository;

    @PutMapping
    public String newJobController(@RequestParam("newJobConfiguration") String jobConfigurationString,
                                   Authentication authentication) {
        User user = ur.findByUsername(authentication.getName()).orElse(null);

        String jobCreated = createNewJob(user, jobConfigurationString);

        JSONObject responseJSON = new JSONObject();
        if (jobCreated.contains("Error:")) {
            responseJSON.put("status", jobCreated);
            return responseJSON.toString();
        }
        responseJSON.put("status", jobCreated);
        return responseJSON.toString();
    }

    @GetMapping
    public String getJobs(Authentication authentication) {
        User user = ur.findByUsername(authentication.getName()).orElse(null);

        Set<Job> userJobs = jobRepository.findJobsByUserOrderById(user);

        JSONArray userJobsJSON = new JSONArray();
        System.out.println("jobs by user: " + userJobs.size() + " " + userJobsJSON);

        for (Job job : userJobs) {
            JSONObject jobJSON = new JSONObject();
            jobJSON.put("id", job.getId());
            jobJSON.put("title", job.getTitle());
            jobJSON.put("description", job.getDescription());
            jobJSON.put("creation", job.getCreationDateTime());
            if (job.getReferenceFile() != null) {
                jobJSON.put("referenceFileId", job.getReferenceFile().getId());
                jobJSON.put("referenceFileName", job.getReferenceFile().getFileName());
            } else {
                jobJSON.put("referenceFileId", "none");
                jobJSON.put("referenceFileName", "none");
            }
            if (job.getReferenceFile() != null) {
                jobJSON.put("ccFileId", job.getCcFile().getId());
                jobJSON.put("ccFileName", job.getCcFile().getFileName());
            } else {
                jobJSON.put("ccFileId", "none");
                jobJSON.put("ccFileName", "none");
            }
            jobJSON.put("JSONConfiguration", job.getJSONConfiguration());

            userJobsJSON.put(jobJSON);
        }

        System.out.println("Users jobs: " + userJobsJSON);
        return userJobsJSON.toString();
    }

    private String createNewJob(User user, String jobConfiguration) {
        JSONObject jobConfigurationJSON = new JSONObject(jobConfiguration);

        Job newJob = new Job();
        jobRepository.save(newJob);
        jobConfigurationJSON.put("jobId", newJob.getId());

        newJob.setJSONConfiguration(jobConfigurationJSON.toString());
        newJob.setUser(user);

        if (jobConfigurationJSON.has("reference")) {
            Long referenceFileId = jobConfigurationJSON.getJSONObject("reference").getLong("fileId");
            JobFile referenceFile = getJobFile(referenceFileId, user);
            if (referenceFile == null) {
                return "Error: given reference file not found on server!";
            }
            newJob.setReferenceFile(referenceFile);
        }
        if (jobConfigurationJSON.has("costCenter")) {
            Long ccFileId = jobConfigurationJSON.getJSONObject("costCenter").getLong("fileId");
            JobFile ccFile = getJobFile(ccFileId, user);
            if (ccFile == null) {
                return "Error: given reference file not found on server!";
            }
            newJob.setCcFile(ccFile);
        }
        jobRepository.save(newJob);
        return "success";
    }

    private JobFile getJobFile(Long id, User user) {
        JobFile jobFile = jobFileRepository.findJobFileByIdAndUser(id, user);
        return jobFile;
    }
}
