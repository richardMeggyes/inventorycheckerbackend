package com.readdeo.inventorychecker.controller;

import com.readdeo.inventorychecker.model.auth.User;
import com.readdeo.inventorychecker.repository.UserRepository;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/user-settings")
@CrossOrigin
public class UserSettingsController {

    @Autowired
    UserRepository ur;

    @DeleteMapping
    public String removeUser(Authentication authentication) {
        User user = ur.findByUsername(authentication.getName()).orElse(null);

        ur.delete(user);

        JSONObject response = new JSONObject();
        response.put("status", "success");

        return response.toString();
    }
}
