package com.readdeo.inventorychecker.controller;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;

@CrossOrigin
@RestController
@RequestMapping("/convertlog")
public class ConvertLogController {

    @GetMapping
    public String getlog() {

        String log = "";

        try{

            Path path = FileSystems.getDefault().getPath("/media/ramdrive", "progress.csv");
            BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8);
            log = reader.readLine();
        } catch (Exception e) {
            System.out.println(e);
        }

//        String log = "" + getRandomInt() + "," + getRandomInt() + "," + getRandomInt() + "," +
//                getRandomInt() + "," + getRandomInt() + "," + getRandomInt() + "," + getRandomInt() + ",";
        return new JSONObject().put("progress", log).toString();
    }

    private int getRandomInt() {
        return new Random().nextInt(120);
    }
}
