package com.readdeo.inventorychecker.repository;

import com.readdeo.inventorychecker.model.Job;
import com.readdeo.inventorychecker.model.auth.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface JobRepository extends JpaRepository<Job, Long> {
    Job findTopByIdAndUser(Long id, User user);

    Set<Job> findJobsByUserOrderById(User user);
}
