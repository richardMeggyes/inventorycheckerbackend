package com.readdeo.inventorychecker.repository;

import com.readdeo.inventorychecker.model.auth.Role;
import com.readdeo.inventorychecker.model.auth.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}