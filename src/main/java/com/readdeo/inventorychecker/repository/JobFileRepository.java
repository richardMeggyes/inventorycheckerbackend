package com.readdeo.inventorychecker.repository;

import com.readdeo.inventorychecker.model.JobFile;
import com.readdeo.inventorychecker.model.auth.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobFileRepository extends JpaRepository<JobFile, Long> {

    JobFile findJobFileById(Long id);

    JobFile findJobFileByIdAndUser(Long id, User user);
}
