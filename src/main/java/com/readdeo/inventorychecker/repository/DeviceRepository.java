//package com.readdeo.inventorychecker.repository;
//
//import com.readdeo.inventorychecker.model.Device;
//import com.readdeo.inventorychecker.model.Job;
//import com.readdeo.inventorychecker.model.auth.User;
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import java.util.Set;
//
//public interface DeviceRepository extends JpaRepository<Device, Long> {
//    Device findFirstById(Long id);
//    Device findByUuid(String uuid);
//    Set<Device> findByUser(User user);
//    Set<Device> findByJobs(Job job);
//}
