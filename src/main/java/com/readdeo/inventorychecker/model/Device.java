//package com.readdeo.inventorychecker.model;
//
//import com.readdeo.inventorychecker.model.auth.User;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import org.hibernate.annotations.CreationTimestamp;
//
//import javax.persistence.*;
//import java.time.LocalDateTime;
//import java.util.HashSet;
//import java.util.Set;
//
//@Entity
//@AllArgsConstructor
//@NoArgsConstructor
//@Data
//@Table(name = "device", schema = "public")
//public class Device {
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column(name = "id")
//    private Long id;
//    @Column(name = "uuid")
//    private String uuid;
//    @CreationTimestamp
//    private LocalDateTime creationDateTime;
//    @ManyToOne
//    private User user;
//    @ManyToOne
//    private User lastLoggedInUser;
//    @Column(name = "name")
//    private String name;
//    @Column(name = "deviceHoldersName")
//    private String deviceHoldersName;
//    @ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(name = "device_jobs",
//            joinColumns = @JoinColumn(name = "device_id"),
//            inverseJoinColumns = @JoinColumn(name = "job_id"))
//    private Set<Job> jobs = new HashSet<>();
//}
