package com.readdeo.inventorychecker.model.auth;

public enum RoleName {
    ROLE_USER,
    ROLE_PM,
    ROLE_ADMIN,
    ROLE_CONVERT,
    ROLE_DEBUG
}