package com.readdeo.inventorychecker.model;

import com.readdeo.inventorychecker.model.auth.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "job_file", schema = "public")
public class JobFile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @ManyToOne
    private User user;
    @Column(name = "fileName")
    private String fileName;
    @Column(name = "dataType")
    private String dataType;
    @Column
    @CreationTimestamp
    private LocalDateTime uploadDateTime;

}
