package com.readdeo.inventorychecker.model;

import com.readdeo.inventorychecker.model.auth.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "job", schema = "public")
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @ManyToOne
    private User user;
    @Column
    @CreationTimestamp
    private LocalDateTime creationDateTime;
    @Column(name = "title")
    private String title = "";
    @Column(name = "description")
    private String description = "";
    @OneToOne
    private JobFile referenceFile;
    @OneToOne
    private JobFile ccFile;
    @Column(name = "job_reference_table_name")
    private String referenceTableName;
    @Column(name = "job_cc_table_name")
    private String ccTableName;
    @Column(name = "JSONConfiguration", columnDefinition = "TEXT")
    private String JSONConfiguration;

}
