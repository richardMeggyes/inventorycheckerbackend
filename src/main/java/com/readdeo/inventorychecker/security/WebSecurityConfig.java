package com.readdeo.inventorychecker.security;

import com.readdeo.inventorychecker.security.jwt.JwtAuthEntryPoint;
import com.readdeo.inventorychecker.security.jwt.JwtAuthTokenFilter;
import com.readdeo.inventorychecker.security.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        prePostEnabled = true
)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    private JwtAuthEntryPoint unauthorizedHandler;

    @Bean
    public JwtAuthTokenFilter authenticationJwtTokenFilter() {
        return new JwtAuthTokenFilter();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().
                authorizeRequests()
                .antMatchers("/login*").permitAll()
                .antMatchers("/signup").permitAll()
                .antMatchers("/api/auth/**").permitAll()
                .antMatchers("/upload").permitAll()
                .antMatchers("/device").permitAll()
                .antMatchers("/converter").hasAuthority("ROLE_CONVERT")
                .antMatchers("/converter-client").hasAuthority("ROLE_CONVERT")
                .antMatchers("/converter").hasAuthority("ROLE_CONVERT")
                .antMatchers("/converter-fileinfo").hasAuthority("ROLE_CONVERT")
                .antMatchers("/convertlog").hasAuthority("ROLE_CONVERT")
                .antMatchers("/converter-job").hasAuthority("ROLE_CONVERT")
                .antMatchers("/converter-progress").hasAuthority("ROLE_CONVERT")
                .antMatchers("/client-name").hasAuthority("ROLE_CONVERT")
                .antMatchers("/converter-projects").hasAuthority("ROLE_CONVERT")
                .anyRequest().permitAll()
                .and()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }
}
