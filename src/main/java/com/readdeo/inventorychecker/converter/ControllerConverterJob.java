package com.readdeo.inventorychecker.converter;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping("/converter-job")
public class ControllerConverterJob {

    private String outputPath = "/home/readdeo/Documents/CONVERT/OUTPUT/";

    @Autowired
    private ServiceConverterProgressInfo converterProgressInfoService;

    @Autowired
    private RepositoryVideoFile videoFileRepository;

    @GetMapping
    public void getFile(@RequestParam("client") String clientId, @RequestParam("fileId") Long fileId, HttpServletResponse response) {

        try {
            VideoFile fileToConvert = videoFileRepository.findFirstByIdAndAssignedDeviceId(fileId, clientId);
            // get your file as InputStream
            File initialFile = new File(fileToConvert.fileFullPath());
            InputStream is = new FileInputStream(initialFile);

            // copy it to response's OutputStream
            org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException ex) {
            // log.info("Error writing file to output stream. Filename was '{}'", fileName, ex);
            throw new RuntimeException("IOError writing file to output stream");
        }
    }

    @PutMapping
    public String newReservation(@RequestParam("client") String clientId) {
        if (converterProgressInfoService.isClientActive(clientId)) {

            Set<VideoFile> videoFilesSet = videoFileRepository.findAllByStatusAndAssignedDeviceId(1, clientId);

            if (videoFilesSet.size() != 0) {
                for (VideoFile videoFile : videoFilesSet) {
                    videoFile.setStatus(3);
                    videoFileRepository.save(videoFile);
                }
            }

            VideoFile fileToConvert = videoFileRepository.findFirstByStatusAndProjectActiveTrue(0);

            if (fileToConvert != null) {
                fileToConvert.setAssignedDeviceId(clientId);
                fileToConvert.setStatus(1);

                videoFileRepository.save(fileToConvert);

                JSONObject response = new JSONObject();
                response.put("status", "success");
                response.put("fileId", fileToConvert.getId());
                response.put("fileName", fileToConvert.getName());
                return response.toString();
            }
        }
        JSONObject response = new JSONObject();
        response.put("status", "inactive");
        return response.toString();
    }

    @PostMapping
    public String handleFileUpload(@RequestParam("fileId") Long fileId,
                                   @RequestParam("client") String clientId,
                                   @RequestParam("file") MultipartFile file) {
        if (clientId.contains(",")) {
            clientId = clientId.split(",")[0];
        }
        VideoFile convertedFile = videoFileRepository.findFirstByIdAndAssignedDeviceId(fileId, clientId);
        String convertedFilePath = convertedFile.getPath().replace("INPUT", "OUTPUT");

        try {
            File uploadedFile = new File(convertedFilePath + "C_" + convertedFile.getName());
            uploadedFile.getParentFile().mkdirs();
            FileUtils.writeByteArrayToFile(uploadedFile, file.getBytes());

            VideoFile fileToConvert = videoFileRepository.findFirstById(fileId);
            fileToConvert.setStatus(2);

            fileToConvert.setOldSize(uploadedFile.length());
            videoFileRepository.save(fileToConvert);

        } catch (IOException e) {
            e.printStackTrace();
            JSONObject response = new JSONObject();
            response.put("status", "failed");
            response.put("message", "failed to upload file");
            response.put("error", e.toString());
            return response.toString();
        }
        JSONObject response = new JSONObject();
        response.put("status", "success");
        return response.toString();
    }
}
