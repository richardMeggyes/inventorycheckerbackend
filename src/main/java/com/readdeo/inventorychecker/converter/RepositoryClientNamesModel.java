package com.readdeo.inventorychecker.converter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;


@Repository
public interface RepositoryClientNamesModel extends JpaRepository<ModelClientNames, Long> {
    ModelClientNames findFirstByUuid(String uuid);
    Set<ModelClientNames> findAllBy();
}
