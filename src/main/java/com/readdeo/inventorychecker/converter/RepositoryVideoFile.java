package com.readdeo.inventorychecker.converter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.LinkedList;
import java.util.Set;


@Repository
public interface RepositoryVideoFile extends JpaRepository<VideoFile, Long> {

    LinkedList<VideoFile> findAllByIdNotNull();
    VideoFile findFirstById(Long id);
    VideoFile findFirstByIdAndAssignedDeviceId(Long id, String clientId);
    Set<VideoFile> findAllByStatusAndAssignedDeviceId(int status, String clientId);
    VideoFile findFirstByStatus(int status);
    VideoFile findFirstByStatusAndProjectActiveTrue(int Status);
    Set<VideoFile> findAllByPath(String path);
    void deleteAll();
}
