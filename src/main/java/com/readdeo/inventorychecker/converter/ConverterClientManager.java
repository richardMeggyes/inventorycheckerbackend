package com.readdeo.inventorychecker.converter;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/converter-client")
public class ConverterClientManager {

    @Autowired
    private ServiceConverterProgressInfo converterProgressInfoService;

    @PutMapping
    public String statusChange(@RequestParam("clientId") String clientId,
                               @RequestParam("operation") String operation) {
        JSONObject response = new JSONObject();
        if (operation.equals("switchActive")) {
            boolean newStatus = converterProgressInfoService.switchClientActiveStatus(clientId);
            response.put("newStatus", newStatus);
        }
        return response.toString();
    }
}
