package com.readdeo.inventorychecker.converter;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/readfiles")
public class ReadFiles {

    @Value("${video.files.path}")
    private String inputPath;


    private String[] videoFormats = {"264", "3g2", "3gp", "3gp2", "3gpp", "3gpp2", "3mm", "3p2", "60d", "787", "89", "aaf", "aec", "aep", "aepx",
            "aet", "aetx", "ajp", "ale", "am", "amc", "amv", "amx", "anim", "aqt", "arcut", "arf", "asf", "asx", "avb",
            "avc", "avd", "avi", "avp", "avs", "avs", "avv", "axm", "bdm", "bdmv", "bdt2", "bdt3", "bik", "bin", "bix",
            "bmk", "bnp", "box", "bs4", "bsf", "bvr", "byu", "camproj", "camrec", "camv", "ced", "cel", "cine", "cip",
            "clpi", "cmmp", "cmmtpl", "cmproj", "cmrec", "cpi", "cst", "cvc", "cx3", "d2v", "d3v", "dat", "dav", "dce",
            "dck", "dcr", "dcr", "ddat", "dif", "dir", "divx", "dlx", "dmb", "dmsd", "dmsd3d", "dmsm", "dmsm3d", "dmss",
            "dmx", "dnc", "dpa", "dpg", "dream", "dsy", "dv", "dv-avi", "dv4", "dvdmedia", "dvr", "dvr-ms", "dvx", "dxr",
            "dzm", "dzp", "dzt", "edl", "evo", "eye", "ezt", "f4p", "f4v", "fbr", "fbr", "fbz", "fcp", "fcproject",
            "ffd", "flc", "flh", "fli", "flv", "flx", "gfp", "gl", "gom", "grasp", "gts", "gvi", "gvp", "h264", "hdmov",
            "hkm", "ifo", "imovieproj", "imovieproject", "ircp", "irf", "ism", "ismc", "ismv", "iva", "ivf", "ivr", "ivs",
            "izz", "izzy", "jss", "jts", "jtv", "k3g", "kmv", "ktn", "lrec", "lsf", "lsx", "m15", "m1pg", "m1v", "m21",
            "m21", "m2a", "m2p", "m2t", "m2ts", "m2v", "m4e", "m4u", "m4v", "m75", "mani", "meta", "mgv", "mj2", "mjp",
            "mjpg", "mk3d", "mkv", "mmv", "mnv", "mob", "mod", "modd", "moff", "moi", "moov", "mov", "movie", "mp21",
            "mp21", "mp2v", "mp4", "mp4v", "mpe", "mpeg", "mpeg1", "mpeg4", "mpf", "mpg", "mpg2", "mpgindex", "mpl",
            "mpl", "mpls", "mpsub", "mpv", "mpv2", "mqv", "msdvd", "mse", "msh", "mswmm", "mts", "mtv", "mvb", "mvc",
            "mvd", "mve", "mvex", "mvp", "mvp", "mvy", "mxf", "mxv", "mys", "ncor", "nsv", "nut", "nuv", "nvc", "ogm",
            "ogv", "ogx", "osp", "otrkey", "pac", "par", "pds", "pgi", "photoshow", "piv", "pjs", "playlist", "plproj",
            "pmf", "pmv", "pns", "ppj", "prel", "pro", "prproj", "prtl", "psb", "psh", "pssd", "pva", "pvr", "pxv",
            "qt", "qtch", "qtindex", "qtl", "qtm", "qtz", "r3d", "rcd", "rcproject", "rdb", "rec", "rm", "rmd", "rmd",
            "rmp", "rms", "rmv", "rmvb", "roq", "rp", "rsx", "rts", "rts", "rum", "rv", "rvid", "rvl", "sbk", "sbt",
            "scc", "scm", "scm", "scn", "screenflow", "sec", "sedprj", "seq", "sfd", "sfvidcap", "siv", "smi", "smi",
            "smil", "smk", "sml", "smv", "spl", "sqz", "srt", "ssf", "ssm", "stl", "str", "stx", "svi", "swf", "swi",
            "swt", "tda3mt", "tdx", "thp", "tivo", "tix", "tod", "tp", "tp0", "tpd", "tpr", "trp", "ts", "tsp", "ttxt",
            "tvs", "usf", "usm", "vc1", "vcpf", "vcr", "vcv", "vdo", "vdr", "vdx", "veg", "vem", "vep", "vf", "vft",
            "vfw", "vfz", "vgz", "vid", "video", "viewlet", "viv", "vivo", "vlab", "vob", "vp3", "vp6", "vp7", "vpj",
            "vro", "vs4", "vse", "vsp", "w32", "wcp", "webm", "wlmp", "wm", "wmd", "wmmp", "wmv", "wmx", "wot", "wp3",
            "wpl", "wtv", "wve", "wvx", "xej", "xel", "xesc", "xfl", "xlmv", "xmv", "xvid", "y4m", "yog", "yuv", "zeg",
            "zm1", "zm2", "zm3", "zmv"};

    @Autowired
    private RepositoryVideoFile videoFileRepository;

    @Autowired
    private RepositoryClientNamesModel clientNamesRepository;

    @Autowired
    private ProjectRepository projectRepository;


    @GetMapping
    public String readFiles() {
        System.out.println("Files location: " + inputPath);

        LinkedList<VideoFile> knownFiles = videoFileRepository.findAllByIdNotNull();
        ArrayList<File> currentFiles = new ArrayList<File>();
        listf(inputPath, currentFiles);

        int deletedFiles = reReadFilesList(knownFiles, currentFiles);

        knownFiles = videoFileRepository.findAllByIdNotNull();

        JSONObject responseJSON = new JSONObject();
        responseJSON.put("deletedFiles", deletedFiles);

        String[] subDirs = getSubDirs(inputPath);

        JSONObject dirsStat = new JSONObject();
        JSONArray dirNames = new JSONArray();
        int counter = 0;
        for (String dir : subDirs) {
            ArrayList<String> data = new ArrayList<String>();
            data.add(dir);

            JSONObject dirName = new JSONObject();
            String projectName = inputPath + dir + "/";
            System.out.println("Projectname: " + projectName);
            int filesNum = filesInFolder(projectName);
            dirName.put(dir, filesNum);
            dirNames.put(dirName);

            Project project = projectRepository.findFirstByName(projectName);
            project.setNumberOfFiles(filesNum);
            projectRepository.save(project);
        }

        responseJSON.put("dirNames", dirNames.toString());
        responseJSON.put("dirStat", dirsStat.toString());
        responseJSON.put("videoFiles", listFiles(knownFiles).toString());
        responseJSON.put("subdirs", arrayToCSV(subDirs));
        System.out.println("Reading files finished " + inputPath);
        return responseJSON.toString();
    }

    private void listf(String directoryName, List<File> files) {
        File directory = new File(directoryName);

        // Get all files from a directory.
        File[] fList = directory.listFiles();
        if (fList != null)
            for (File file : fList) {
                if (file.isFile()) {
                    String[] nameSplitted = file.getName().split("\\.");
                    String fileExtension = nameSplitted[nameSplitted.length - 1].toLowerCase();
                    if (Arrays.stream(videoFormats).anyMatch(fileExtension::equals)) {
                        files.add(file);
                    } else {
                        File destFile = new File(file.getAbsolutePath().replace(File.separator + "INPUT" + File.separator, File.separator + "OUTPUT" + File.separator));
                        destFile.getParentFile().mkdirs();
                        if(file.renameTo(destFile))
                        {
                            // if file copied successfully then delete the original file
                            file.delete();
                        }
                        else
                        {
                            System.out.println("Failed to move the file " + file.getAbsolutePath());
                        }
                    }
                } else if (file.isDirectory()) {
                    listf(file.getAbsolutePath(), files);
                }
            }
    }

    private int filesInFolder(String directoryName) {
        File directory = new File(directoryName);

        int fileCounter = 0;

        // Get all files from a directory.
        File[] fList = directory.listFiles();
        if (fList != null)
            for (File file : fList) {
                if (file.isFile()) {
                    String[] nameSplitted = file.getName().split("\\.");
                    String fileExtension = nameSplitted[nameSplitted.length - 1].toLowerCase();
                    if (Arrays.stream(videoFormats).anyMatch(fileExtension::equals)) {
                        fileCounter++;
                    }
                }
            }
        return fileCounter;
    }

    private int removeMissingFilesFromDb(ArrayList<File> files, LinkedList<VideoFile> knownFiles) {
        int deleteCounter = 0;

        for (VideoFile videoFile : knownFiles) {
            boolean fileExists = false;
            String videoFileFullPath = videoFile.getPath() + videoFile.getName();
            for (File file : files) {
                if (videoFileFullPath.equals(file.getPath())) {
                    fileExists = true;
                }
            }
            if (!fileExists) {
                Project project = videoFile.getProject();
                project.setNumberOfFiles(project.getNumberOfFiles() - 1);
                projectRepository.save(project);
                videoFileRepository.delete(videoFile);
                deleteCounter++;
            }
        }
        System.out.println("Removed files from db: " + deleteCounter);
        return deleteCounter;
    }

    private int reReadFilesList(LinkedList<VideoFile> knownFiles, ArrayList<File> currentFiles) {
        int deletedFiles = removeMissingFilesFromDb(currentFiles, knownFiles);

        for (File file : currentFiles) {
            boolean fileIsKnown = false;

            for (VideoFile videoFile : knownFiles) {
                String videoFileFullPath = videoFile.getPath() + videoFile.getName();
                if (file.getPath().equals(videoFileFullPath)) {
                    fileIsKnown = true;
                }
            }
            if (!fileIsKnown) {
                VideoFile newVideoFile = new VideoFile();
                newVideoFile.setName(file.getName());
                newVideoFile.setPath((file.getPath()).replace(file.getName(), ""));
                newVideoFile.setStatus(0);

                File videoFileFile = new File(newVideoFile.getPath(), newVideoFile.getName());
                newVideoFile.setNewSize(videoFileFile.length());

                String projectName = newVideoFile.getPath().replace("/home/readdeo/Documents/CONVERT/INPUT/", "");
                if (projectName.equals("")) {
                    projectName = "root";
                }
                try {
                    Project project = projectRepository.findFirstByName(projectName);
                    project.setNumberOfFiles(project.getNumberOfFiles() + 1);
                    projectRepository.save(project);
                    newVideoFile.setProject(project);
                } catch (NullPointerException e) {
                    Project project = new Project();
                    project.setName(projectName);
                    project.setActive(false);
                    projectRepository.save(project);
                    Project projectLookedUp = projectRepository.findFirstByName(projectName);
                    newVideoFile.setProject(projectLookedUp);
                }
                videoFileRepository.save(newVideoFile);
            }
        }
        return deletedFiles;
    }

    private JSONArray listFiles(LinkedList<VideoFile> knownFiles) {
        JSONArray videoFiles = new JSONArray();
        for (VideoFile videoFile : knownFiles) {
            JSONObject videoFileJSON = new JSONObject();
            videoFileJSON.put("id", videoFile.getId());
            videoFileJSON.put("name", videoFile.getName());
            videoFileJSON.put("path", videoFile.getPath().replace(inputPath, ""));
            videoFileJSON.put("status", videoFile.getStatus());
            videoFileJSON.put("addedOn", videoFile.getAddedOn());
            videoFileJSON.put("device", videoFile.getAssignedDeviceId());
            try {
                String deviceName = clientNamesRepository.findFirstByUuid(videoFile.getAssignedDeviceId()).getName();
                videoFileJSON.put("deviceName", deviceName);
            } catch (NullPointerException e) {
                videoFileJSON.put("deviceName", "NotFound");
            }

            videoFiles.put(videoFileJSON);
        }
        return videoFiles;
    }

    private String[] getSubDirs(String path) {
        File file = new File(path);
        String[] directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });
        return directories;
    }

    private String arrayToCSV(String[] list) {
        String csvStr = "";
        for (String str : list) {
            csvStr += str + ",";
        }
        if (csvStr.length() == 0) {
            return "";
        }
        return csvStr.substring(0, csvStr.length() - 1);
    }
}
