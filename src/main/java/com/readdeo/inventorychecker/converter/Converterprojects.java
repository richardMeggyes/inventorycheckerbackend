package com.readdeo.inventorychecker.converter;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping("/converter-projects")
public class Converterprojects {

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping
    public String getProjects() {
        // @RequestParam Map<String,String> allRequestParams,
        JSONObject response = new JSONObject();
        try {

            Set<Project> projects = projectRepository.findAllBy();
            response.put("projects", projects);
            response.put("status", "success");
        } catch (Exception e) {
            response.put("status", "error");
            response.put("exception", e);
        }
        return response.toString();
    }

    @PostMapping
    public String switchProjectActive(@RequestParam("projectId") Long projectId) {
        JSONObject response = new JSONObject();
        Project project = projectRepository.findFirstById(projectId);
        project.setActive(!project.isActive());

        response.put("projectId", projectId);
        response.put("projectStatus", project.isActive());
        projectRepository.save(project);
        response.put("status", "success");
        return response.toString();
    }
}
