package com.readdeo.inventorychecker.converter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "projectName", schema = "public")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;
    @Column
    @CreationTimestamp
    private LocalDateTime addedOn;
    @Column
    private String name;
    @Column
    private boolean active = false;
    @Column
    private int numberOfFiles = 1;
}
