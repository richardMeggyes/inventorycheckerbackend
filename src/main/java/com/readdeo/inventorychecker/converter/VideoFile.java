package com.readdeo.inventorychecker.converter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table
public class VideoFile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;
    @Column
    @CreationTimestamp
    private LocalDateTime addedOn;
    @Column
    private String path;
    @Column
    private String name;
    @Column
    private int status;
    @Column
    private String assignedDeviceId;
    @Column
    private int tested = 0;
    @Column
    private String testResults;
    @ManyToOne
    private Project project;
    @Column
    private Long oldSize;
    @Column
    private Long newSize;
    @Column
    private Long frames;

    public String fileFullPath() {
        String slash = "";
        if (!path.substring(path.length() - 1).equals("/")) {
            slash = "/";
        }
        return path + slash + name;
    }
}
