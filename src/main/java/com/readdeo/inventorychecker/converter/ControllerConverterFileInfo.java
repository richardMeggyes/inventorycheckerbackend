package com.readdeo.inventorychecker.converter;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;

@RestController
@CrossOrigin
@RequestMapping("/converter-fileinfo")
public class ControllerConverterFileInfo {

    @Autowired
    private RepositoryVideoFile videoFileRepository;

    @GetMapping
    public String getFilesWithInfo() {
        LinkedList<VideoFile> videoFiles = videoFileRepository.findAllByIdNotNull();

        JSONArray videoFilesJSONArray = new JSONArray();

        for (VideoFile file : videoFiles) {
            JSONObject videoFileJSON = new JSONObject();
            videoFileJSON.put("name", file.getName());
            videoFileJSON.put("path", file.getPath());
            videoFileJSON.put("addedOn", file.getAddedOn());
            videoFileJSON.put("status", file.getStatus());
            videoFileJSON.put("assignedId", file.getAssignedDeviceId());
            videoFileJSON.put("tested", file.getTested());
            videoFileJSON.put("testResults", file.getTestResults());
            videoFilesJSONArray.put(videoFileJSON);
        }
        return videoFilesJSONArray.toString();
    }
}
