package com.readdeo.inventorychecker.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/client-name")
public class ClientName {

    @Autowired
    private RepositoryClientNamesModel clientNamesRepository;

    @PostMapping
    public void setClientsName(@RequestParam("name") String name,
                         @RequestParam("clientId") String clientId) {
        try{
            ModelClientNames clientName = clientNamesRepository.findFirstByUuid(clientId);
            clientName.setName(name);
            clientNamesRepository.save(clientName);
        }
        catch (NullPointerException e) {
            ModelClientNames clientName = new ModelClientNames();
            clientName.setName(name);
            clientNamesRepository.save(clientName);
        }
    }
}
