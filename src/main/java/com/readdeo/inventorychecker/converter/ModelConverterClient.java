package com.readdeo.inventorychecker.converter;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModelConverterClient {
    private String name;
    private String id;
    private LocalDateTime lastSeen;
    private Double fps;
    private Long frame;
    private Long totalFrames;
    private String remainingTime;
    private String elapsedTime;
    private boolean active = true;
}
