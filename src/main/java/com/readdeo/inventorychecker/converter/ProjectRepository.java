package com.readdeo.inventorychecker.converter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.Set;


@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    Project findFirstByName(String name);
    Project findFirstById(Long id);
    Set<Project> findAllByName(String name);
    Set<Project> findAllBy();
    Set<Project> findAllByActiveTrue();
    void deleteAll();
}
