package com.readdeo.inventorychecker.converter;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/converter-progress")
public class ControllerConverterProgressInfo {

    @Autowired
    private ServiceConverterProgressInfo converterProgressInfoService;

    @Autowired
    private RepositoryClientNamesModel clientNamesRepository;

    @Autowired
    private RepositoryVideoFile videoFileRepository;

    @PostMapping
    public String updateProgress(
            @RequestParam("fps") Double fps,
            @RequestParam("frame") Long frame,
            @RequestParam("totalFrames") Long totalFrames,
            @RequestParam("client") String clientId,
            @RequestParam("remainingTime") String remainingTime,
            @RequestParam("elapsedTime") String elapsedTime) {

        converterProgressInfoService.updateClientProgress(fps, frame, totalFrames, clientId, remainingTime, elapsedTime);

        try {
            clientNamesRepository.findFirstByUuid(clientId);
        } catch (NullPointerException e) {
            ModelClientNames clientName = new ModelClientNames();
            clientName.setUuid(clientId);
            clientNamesRepository.save(clientName);
        }

        JSONObject response = new JSONObject();
        response.put("status", "success");
        return response.toString();
    }

    @GetMapping
    public String getProgressInfo() {
        return converterProgressInfoService.getClientsProgesses().toString();
    }

    @PutMapping
    public String statusChange(@RequestParam("videoFileId") Long videoFileId) {
        // @RequestParam Map<String,String> allRequestParams,
        JSONObject response = new JSONObject();
        VideoFile videoFile = videoFileRepository.findFirstById(videoFileId);
        try {
            int status = videoFile.getStatus();
            if (status < 2) {
                videoFile.setStatus(status + 1);
                videoFileRepository.save(videoFile);
            } else {
                videoFile.setStatus(0);
                videoFileRepository.save(videoFile);
            }
            response.put("status", "success");
            response.put("newStatus", videoFileRepository.findFirstById(videoFileId).getStatus());
        } catch (Exception e) {
            response.put("status", "error");
            response.put("exception", e);
        }
        return response.toString();
    }

    @DeleteMapping
    public String clearClients() {
        JSONObject response = new JSONObject();
        converterProgressInfoService.removeClients();
        response.put("status", "success");
        return response.toString();
    }
}
