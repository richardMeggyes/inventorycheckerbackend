package com.readdeo.inventorychecker.converter;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Set;

@Service
public class ServiceConverterProgressInfo {

    @Autowired
    private RepositoryClientNamesModel clientNamesRepository;

    private ArrayList<ModelConverterClient> clients = new ArrayList<ModelConverterClient>();

    @PostConstruct
    public void restoreClients() {
        Set<ModelClientNames> storedClientNames = clientNamesRepository.findAllBy();
        for (ModelClientNames storedClientName : storedClientNames) {
            ModelConverterClient client = new ModelConverterClient();
            client.setId(storedClientName.getUuid());
            client.setName(storedClientName.getName());
            client.setLastSeen(LocalDateTime.of(1970, 1, 1, 0, 0, 0));
            clients.add(client);
        }
    }

    public void updateClientProgress(Double fps, Long frame, Long totalFrames, String clientId, String remaining, String elapsed) {

        boolean clientUpdated = false;

        for (ModelConverterClient client : clients) {

            if (client.getId().equals(clientId)) {
                client.setFps(fps);
                client.setFrame(frame);
                client.setTotalFrames(totalFrames);
                client.setLastSeen(LocalDateTime.now());
                client.setElapsedTime(elapsed);
                client.setRemainingTime(remaining);
                clientUpdated = true;
            }
        }

        if (!clientUpdated) {
            ModelConverterClient client = new ModelConverterClient();
//            client.setName(clientId);
            client.setLastSeen(LocalDateTime.now());
            client.setTotalFrames(totalFrames);
            client.setFrame(frame);
            client.setFps(fps);
            client.setId(clientId);
            client.setRemainingTime(remaining);
            client.setElapsedTime(elapsed);
            clients.add(client);

            ModelClientNames newClientname = new ModelClientNames();
            newClientname.setUuid(clientId);
            newClientname.setName(clientId);
            clientNamesRepository.save(newClientname);
        }
    }

    public JSONArray getClientsProgesses() {
        JSONArray clientsJSONArray = new JSONArray();

        for (ModelConverterClient client : clients) {
            clientsJSONArray.put(clientInfoToJSONObject(client));
        }
        return clientsJSONArray;
    }

    public void removeClients() {
        clients.clear();
        restoreClients();
    }

    public boolean switchClientActiveStatus(String clientId) {
        for (ModelConverterClient client : clients) {
            if (client.getId().equals(clientId)) {
                client.setActive(!client.isActive());
                return client.isActive();
            }
        }
        throw new NoSuchElementException();
    }

    public boolean isClientActive(String clientId) {
        for (ModelConverterClient client : clients) {
            if (client.getId().equals(clientId)) {
                return client.isActive();
            }
        }
        return true;
    }

    public void setClientName(String clientId, String name) {

        ModelClientNames clientName = clientNamesRepository.findFirstByUuid(clientId);
        clientName.setName(name);

        for (ModelConverterClient client : clients) {
            if (client.getId().equals(clientId)) {
                client.setName(name);
            }
        }
    }

    private JSONObject clientInfoToJSONObject(ModelConverterClient client) {
        JSONObject clientJSON = new JSONObject();
        clientJSON.put("id", client.getId());
        clientJSON.put("name", client.getName());
        clientJSON.put("elapsedTime", client.getElapsedTime());
        clientJSON.put("remainingTime", client.getRemainingTime());
        clientJSON.put("fps", client.getFps());
        clientJSON.put("frame", client.getFrame());
        clientJSON.put("totalFrames", client.getTotalFrames());
        clientJSON.put("lastSeen", client.getLastSeen().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        clientJSON.put("isActive", client.isActive());
        return clientJSON;
    }
}
