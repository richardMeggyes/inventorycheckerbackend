package com.readdeo.authentication.authenticationmicroservice.repository;

import com.readdeo.authentication.authenticationmicroservice.model.auth.Role;
import com.readdeo.authentication.authenticationmicroservice.model.auth.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}