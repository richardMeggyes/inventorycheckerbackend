package com.readdeo.authentication.authenticationmicroservice.model.auth;

public enum  RoleName {
    ROLE_USER,
    ROLE_PM,
    ROLE_ADMIN
}