package com.readdeo.authentication.authenticationmicroservice.controller;

import com.readdeo.authentication.authenticationmicroservice.message.request.LoginForm;
import com.readdeo.authentication.authenticationmicroservice.message.request.SignUpForm;
import com.readdeo.authentication.authenticationmicroservice.message.response.JwtResponse;
import com.readdeo.authentication.authenticationmicroservice.message.response.ResponseMessage;
import com.readdeo.authentication.authenticationmicroservice.model.auth.Role;
import com.readdeo.authentication.authenticationmicroservice.model.auth.RoleName;
import com.readdeo.authentication.authenticationmicroservice.model.auth.User;
import com.readdeo.authentication.authenticationmicroservice.repository.RoleRepository;
import com.readdeo.authentication.authenticationmicroservice.repository.UserRepository;
import com.readdeo.authentication.authenticationmicroservice.security.jwt.JwtProvider;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthRestAPIs {

	@Autowired
    AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
    PasswordEncoder encoder;

	@Autowired
	JwtProvider jwtProvider;

	@GetMapping("/login")
	public ResponseEntity<?> authenticateUser(@Valid @RequestParam("user") String username, @RequestParam("password") String password) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(username, password));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtProvider.generateJwtToken(authentication);

		JSONObject loginResponse = new JSONObject();
		loginResponse.put("loggedin", "true");
		loginResponse.put("jwt", jwt);
		return new ResponseEntity<>(new ResponseMessage(loginResponse.toString()), HttpStatus.OK);
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Username is already taken!"),
					HttpStatus.BAD_REQUEST);
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Email is already in use!"),
					HttpStatus.BAD_REQUEST);
		}

		// Creating user's account
		User user = new User(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
				encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		strRoles.forEach(role -> {
			switch (role) {
			case "admin":
				Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(adminRole);

				break;
			case "pm":
				Role pmRole = roleRepository.findByName(RoleName.ROLE_PM)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(pmRole);

				break;
			default:
				Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(userRole);
			}
		});

		user.setRoles(roles);
		userRepository.save(user);

		return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);
	}

	@GetMapping("/validatejwt")
	public ResponseEntity<?> validateJWT(@Valid @RequestParam("jwt") String jwtToken) {
		System.out.println("jwtToken: " + jwtToken);
		boolean jwtValid = jwtProvider.validateJwtToken(jwtToken);
		System.out.println("JWT validation: " + jwtValid + " " + jwtToken);

		if (jwtValid){
			JSONObject jwtDetails = new JSONObject();
			jwtDetails.put("valid", "true");
			System.out.println("GETJUZERNÉM: " + jwtProvider.getUserNameFromJwtToken(jwtToken));
			jwtDetails.put("username", jwtProvider.getUserNameFromJwtToken(jwtToken));
			System.out.println("jwtDetails: " + jwtDetails);
			return new ResponseEntity<>(new ResponseMessage(jwtDetails.toString()), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseMessage("invalid"), HttpStatus.OK);
		}
	}
}