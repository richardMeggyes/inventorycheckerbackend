package com.readdeo.devicemanagement.devicemanagement.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "device", schema = "public")
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "uuid")
    private String uuid;
    @CreationTimestamp
    private LocalDateTime creationDateTime;
    @Column
    private Long userId;
    @Column
    private Long lastLoggedInUserId;
    @Column(name = "name")
    private String name;
    @Column(name = "deviceHoldersName")
    private String deviceHoldersName;
    @Column
    private Long[] jobs = new Long[]{};
}
