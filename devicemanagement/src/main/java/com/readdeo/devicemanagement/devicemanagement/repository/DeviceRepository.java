package com.readdeo.devicemanagement.devicemanagement.repository;

import com.readdeo.devicemanagement.devicemanagement.model.Device;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface DeviceRepository extends JpaRepository<Device, Long> {
    Device findFirstById(Long id);

    Device findFirstByUuid(String uuid);

    Set<Device> findAllByUserId(Long userId);
}
