package com.readdeo.devicemanagement.devicemanagement.controller;

import com.readdeo.devicemanagement.devicemanagement.model.Device;
import com.readdeo.devicemanagement.devicemanagement.repository.DeviceRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RequestMapping("/device-management")
@RestController
public class DeviceManagementController {

    @Autowired
    DeviceRepository deviceRepository;

    @GetMapping
    public String getDevices(@RequestParam("user") Long userId) {

        Set<Device> devices = deviceRepository.findAllByUserId(userId);

        JSONArray deviceJSONArray = new JSONArray();

        for (Device device : devices) {
            JSONObject deviceJSON = new JSONObject();
            deviceJSON.put("id", device.getId());
            deviceJSON.put("uuid", device.getUuid());
            deviceJSON.put("name", device.getName());
            deviceJSON.put("deviceHolder", device.getDeviceHoldersName());
            deviceJSON.put("lastLoggedInUserId", device.getLastLoggedInUserId());
            deviceJSON.put("jobs", device.getJobs());

            deviceJSONArray.put(deviceJSON);
        }
        return deviceJSONArray.toString();
    }

    @PutMapping
    public String newDevice(@RequestParam("user") Long userId,
                            @RequestParam("uuid") String uuid,
                            @RequestParam("name") String name,
                            @RequestParam("deviceholdersname") String deviceHoldersName) {

        Device newDevice = new Device();
        newDevice.setUuid(uuid);
        newDevice.setName(name);
        newDevice.setUserId(userId);
        newDevice.setDeviceHoldersName(deviceHoldersName);

        deviceRepository.save(newDevice);

        return new JSONObject().put("status", "success").toString();
    }
}
